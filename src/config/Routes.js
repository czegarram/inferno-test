import Inferno from 'inferno';
import {Router, Route, IndexRoute} from 'inferno-router';
import createBrowserHistory from 'history/createBrowserHistory';

import Home from '../app/components/home/Home'
import Entity from '../app/entities/Entity'
import App from '../App'


const browserHistory = createBrowserHistory();

export const routes = (
    <Router history={ browserHistory }>
        <Route component={ App }>
            <IndexRoute component={ Home }/>
            <Route path="/tickets" component={ Entity }></Route>
        </Route>
    </Router>)
