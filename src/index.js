import Inferno from "inferno";
import "./index.css";
import "jquery/jquery.min";
import "bootstrap/dist/js/bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import {routes} from "./config/Routes";

Inferno.render(
    routes,
    document.getElementById('app')
);
