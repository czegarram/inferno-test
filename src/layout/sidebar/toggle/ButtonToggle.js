import Inferno from 'inferno';
import Component from 'inferno-component'

class ButtonToggle extends Component {

    constructor(props) {
        super(props);
        this.state = {active: false};
    }

    toggleSidebar(){
        console.log('Clicked')
        var toggleDiv = document.getElementById('main-content');
        // toggleDiv.('active')
        // this.setState( { condition : !this.state.condition });
    }

    render(){
        return (
                <button type="button" class="btn btn-primary btn-xs"
                        data-toggle="offcanvas" onClick={this.toggleSidebar()}>
                    <i class="glyphicon glyphicon-chevron-left"></i>
                </button>
        );
    }
}

export default ButtonToggle;