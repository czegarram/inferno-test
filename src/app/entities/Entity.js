import Inferno from 'inferno';
import Component from 'inferno-component'

import EntityService from './EntityService'
import Loading from '../components/loading/Loading'
import TicketList from '../components/TicketList/TicketList'

class Entity extends Component {

    componentDidMount() {
        // GET list of dinosaurs from API
        EntityService.getTicketList()
            .then(
                res => {
                    // Set state with fetched dinos list
                    this.setState({
                        tickets: res
                    });
                },
                error => {
                    // An error occurred, set state with error
                    this.setState({
                        error: error
                    });
                }
            );
    }

    render(props, state) {
        return (
            <div className="App card">
                <header className="App-header">
                    <h1 className="text-center">Tickets</h1>
                </header>
                <div className="App-content container-fluid">
                    <div className="row">
                        {
                            state.tickets ? (
                                <TicketList tickets={state.tickets} />
                            ) : (
                            <Loading error={state.error} />
                            )
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default Entity;