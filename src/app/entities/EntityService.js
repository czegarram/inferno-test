
const API = 'http://192.168.99.100:9000/';

// GET list of all dinosaurs from API
function getTicketList() {
    return fetch(`${API}tickets`)
        .then(_verifyResponse, _handleError);
}

// GET a dinosaur's detail info from API by ID
function getTicket(code) {
    return fetch(`${API}ticket/${code}/detail`)
        .then(_verifyResponse, _handleError);
}

// Verify that the fetched response is JSON
function _verifyResponse(res) {
    let contentType = res.headers.get('content-type');

    if (contentType && contentType.indexOf('application/json') !== -1) {
        return res.json();
    } else {
        _handleError({ message: 'Response was not JSON'});
    }
}

// Handle fetch errors
function _handleError(error) {
    console.error('An error occurred:', error);
    throw error;
}

// Export EntityService
const EntityService = { getTicketList, getTicket };
export default EntityService;