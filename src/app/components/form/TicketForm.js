import Inferno from 'inferno';
import Component from 'inferno-component';

class TicketForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            approver: '',
            comment: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {

        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({[name] : value});

        console.log(event)
    }

    handleSubmit(event) {
        alert('A ticket was submitted: ' + this.state.approver + ' with comment ' + this.state.comment);
        //this.state = {value: ''};
        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    Approver:
                    <input name="approver" type="text" value={this.state.approver} onInput={this.handleChange} />
                </label>
                <label>
                    Comment:
                    <input name="comment" type="text" value={this.state.comment} onInput={this.handleChange} />
                </label>
                <input type="submit" value="Submit" />
            </form>
        );
    }
}

export default TicketForm;
