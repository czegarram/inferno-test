import Inferno from 'inferno';

import Component from 'inferno-component';
import Logo from '../../../logo.js';

import './Home.css'


class Home extends Component {

    render() {
        return (<div className=" App App-header">
            <Logo width="80" height="80"/>
            <h2>Welcome to Inferno</h2>
        </div>)
    }
}

export default Home;